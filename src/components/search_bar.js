import React, { Component } from 'react';

class SearchBar extends Component {

    constructor(props) {
        super(props);

        // initialise state
        this.state = { term: ''};

        // ES6 classes no longer autobind this to non React methods
        //this.onInputChange = this.onInputChange.bind(this); // binding not required for this callback
    }

    render() {
        return (
        <div className="input-group search-bar">
            <input
                className="form-control"
                type="text"
                placeholder="Search"
                value={this.state.term}
                onChange={event => this.onInputChange(event.target.value)}/>
            <span className="input-group-addon"><i className="fa fa-search"></i></span>
        </div>
        );
    }

    onInputChange(term) {
        this.setState({term});
        this.props.onSearchTermChange(term);    // notify parent on search term change
    }
}


export default SearchBar;
