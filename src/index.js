import React, { Component }from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import YouTubeAPI from 'youtube-api-search';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
import config from 'config';


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            videos: [],
            selectedVideo: null
        };

        // search youtube for reactjs
        this.videoSearch('reactjs');
    }

    render() {
        const videoSearch = _.debounce((term) => { this.videoSearch(term)},500);
        return (
            <div className="row">
                <div className="col-md-6">
                    {/*<SearchBar onSearchTermChange={term => this.videoSearch(term)}/>*/}
                    <SearchBar onSearchTermChange={videoSearch}/>
                </div>
                <div className="col-md-8">
                    <VideoDetail video={this.state.selectedVideo} />
                </div>
                <div className="col-md-4">
                    <VideoList
                        onVideoSelect={selectedVideo => this.setState({selectedVideo})}
                        videos={this.state.videos} />
                </div>
            </div>
        );
    }

    videoSearch(term) {

        YouTubeAPI({key: config.youtube.api_key, term: term}, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
            console.log(videos);
        });
    }

}


ReactDOM.render(<App />, document.querySelector('.container'));
